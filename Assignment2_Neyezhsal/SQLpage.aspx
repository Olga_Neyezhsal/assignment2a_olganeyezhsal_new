﻿<%@ Page Title="SQL" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQLpage.aspx.cs" Inherits="Assignment2_Neyezhsal.SQLpage" %>
<asp:Content ContentPlaceHolderID="sqlContent" runat="server">
     
    <asp:Image class="sql-image" runat="server" ImageUrl="Resources/sql-select.jpg" alt="sql"/>

        <h3 class="homeTitle" >SQL SELECT STATEMENT </h3> <!--http://www.sql-tutorial.net/SQL-SELECT.asp-->

        <p class="text-content">The SQL SELECT statement is used to select data from a SQL database table. This is usually the very first SQL command every SQ
            L newbie learns and this is because the SELECT SQL statement is one of the most used SQL commands.
        <p class="sideSubTitle"> SELECT syntax:</p>
        <p>The SQL SELECT statement is used to select data from a SQL database table. This is usually the very first SQL command every SQL newbie learns and this is 
            because the SELECT SQL statement is one of the most used SQL commands.</p>
        </p>
        <table class="sql-example">
            <tr>
                <td class="text-content">SELECT * FROM Clients;</td>
            </tr>
        </table>

       <!--my example-->

    <p class="subTitle">ORDER BY</p>
    <p>If you want to get the result in an ordered form by any criterion. The default order is ASC (from low to high). For reverse use DESC.</p>
    <h4 class="sideSubTitle">Example:</h4>
    <table class="sql-example">
        <tr>
            <td class="text-content">SELECT clientlname, clientphone, clientcredits FROM clients order by clientcredits desc;</td>
        </tr>
    </table>
    <img class="order-by" src="Resources/order by.png" alt="select" />
</asp:Content>
 
<asp:Content ContentPlaceHolderID="SideBar" runat="server">
</asp:Content>
