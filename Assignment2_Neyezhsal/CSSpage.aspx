﻿<%@ Page Title="CSS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSSpage.aspx.cs" Inherits="Assignment2_Neyezhsal.CSSpage" %>

<asp:Content  ContentPlaceHolderID="cssContent" runat="server">
    <p class="homeTitle">CASCADING STYLES SHEET</p>
      <img class="layout" src="Resources/layout.png" alt="CSS" />
    <p class="text-content">CSS (Cascading Styles Sheet) or Cascading Style Sheets is a technology that allows you to define styles for HTML documents.
      Due to the fact that HTML is content or content, cascading styles are its visual presentation.</p><br />
 
    <p class="text-content">Almost every site uses a property that creates a frame around an element. It is needed either for buttons or for blocks with text. 
     To create a border, an element in CSS has two properties: a border and a outline.</p>

    <h3 class="subTitle">Border</h3>

    <p>This property is needed to set the frame around the element, indicates its border in the web document, the width of the frame is taken into account when the element is located. 
      It has 3 values ​​- color, thickness and frame type.</p>
    <p class="sideSubTitle">The syntax for the border property is as follows:</p><br />
   <table class="css-example"><td class="text-content">#block {border: 3px solid # 0085ss; / * set the line to be 3 pixels blue * /}</td></table>
    <p>If you want to install one or two or three frames on a certain side, then we indicate this:</p>
  <ul>
    <li>border-top - frame on top;</li>
    <li>border-bottom - frame below;</li>
    <li>border-left - the frame on the left;</li>
    <li>border-right - the frame on the right;</li>
  </ul>
    <table class="css-example">

        <td class="text-content">.block{<br />border-right: 3px solid # 0085cc; border-bottom: 2px dashed # 0085cc;<br />}</td>
    </table>
    
    <p>If you want to remove the borders of the element in CSS, then write in the property border:none</p>
    <h3 class="subTitle">Outline</h3>
    <p>Outline is a property that is needed to set the outer frame of an element.</p><br />
    <p>There are two differences from the border:<br />
     Firstly, the line defined in outline will NOT affect the position of the block itself, its width and height.<br />
     Secondly, the ability to install the frame on a particular side is missing.<br />
     The syntax is the same as the border.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
</asp:Content>