﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2_Neyezhsal._Default" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
   
    <div class="bodyContent">
  <!--slider Christine's code-->
        <div class="slider">
        <h2 class="homeTitle">ONLINE PROGRAMMING TRAINING TEST123</h2>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item item active">
          <img class="cubes" src="Resources/cubes.jpg" alt="First slide">
        </div>
        <div class="carousel-item item">
          <img class="programs" src="Resources/programs.jpg" alt="Second slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>
        </div>
    <!--https://coder-booster.ru/learning-->
    <div class="row">
            <div class="col-sm-8 col-md-4">
                <p class="subTitle">Scalable Solutions</p>
                <p class="text-content">Some courses are aimed at beginners who learn to programme from scratch.
                  Materials are well structured and lead from the basics to more complex things. 
                  All terms are revealed in them, and analysis of common problems is given.</p>  
              </div>
            <div class="col-sm-8 col-md-4">
                <p class="subTitle">Programming for beginners</p>
                <p class="text-content">Courses are fully adapted to modern realities. Saved all the terminology 
                    and professional slang. To make online learning interesting, we try to get rid of the boring university presentation.</p>   
            </div>
            <div class="col-sm-8 col-md-4">
                <p class="subTitle">Completeness</p>
                <p class="text-content">Free - does not mean bad. We are proud of what we do. All online programming courses are carefully
                  thought out. Over time, as well as under the influence of new factors, the materials are adjusted and supplemented.</p> 
            </div>
      </div>
</asp:Content>
