﻿<%@ Page Title="JS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JSpage.aspx.cs" Inherits="Assignment2_Neyezhsal.JSpage" %>
<asp:Content  ContentPlaceHolderID="jsContent" runat="server">
   
    <!--https://www.codecademy.com/courses/introduction-to-javascript/lessons/introduction-to-javascript/exercises/intro?action=resume_content_item-->

    <p class="homeTitle">WHAT IS JAVA SCRIPT?</p>
       <asp:Image class="JS_image" ImageUrl="Resources/javascript.jpg" runat="server" alt="JS"/>

         <p class="text-content">JavaScript is a powerful, flexible, and fast programming language now being used for increasingly complex web development and beyond!</p>

    <!--my example-->
    <p class="subTitle">SWITCH :</p>
    <p class="text-content">The switch javascript statement is used to test a variable for a set of values:</p>
    <table class="js-example">
        <tr>
            <td class="text-content">switch (expression) {<br />case 1://..block of operators ..<br />break <br />case 2://..block of operators .. <br />break<br />[default://..block of operators ..]<br />}</td>
        </tr>
    </table>
    <p class="sideSubTitle">Example:</p>
    <table class="js-example">
        <tr>
            <td class="text-content">var a = 2;<br />switch (a)<br />{<br />case 0:<br />
                case 1:<br />alert ("Zero or one");<br />break;<br />case 2:<br />alert ("Two");<br />
                break;<br />default:<br />alert ("a lot");<br />}</td>
        </tr>
    </table>
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">  
</asp:Content>